import rc from 'rc';

const APP_NAME = 'TWITTER_APP';
const mongoPort = process.env.MONGO_PORT || 27017;

const SESSION = {
  COOKIE_KEY: 'thisappisawesome',
};

const config = {
  port: process.env.PORT || 3000,
  mongo: {
    url: process.env.DATABASE_URL || `mongodb://localhost:${mongoPort}/twitter_app`,
  },
  ...SESSION,
};

export default rc(APP_NAME, config);
