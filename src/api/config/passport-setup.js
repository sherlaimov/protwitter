import passport from 'koa-passport';
import TwitterStrategy from 'passport-twitter';
import User from '../db/models/user';
import config from './config';

// serialize the user.id to save in the cookie session
// so the browser will remember the user when login
passport.serializeUser((user, done) => {
  console.log('serializeUser');
  console.log(user);
  done(null, user.id);
});

// deserialize the cookieUserId to user in the database
passport.deserializeUser(async (id, done) => {
  console.log('***** DESERIALIZE USER ***********');
  console.log({ id });
  await User.findById(id)
    .then(user => {
      // console.log(user);
      done(null, user);
    })
    .catch(e => {
      done(new Error('Failed to deserialize a user'));
    });
});

passport.use(
  new TwitterStrategy(
    {
      consumerKey: config.TWI_CONSUMER_KEY,
      consumerSecret: config.TWI_CONSUMER_SECRET,
      callbackURL: '/auth/twitter/redirect'
    },
    async (token, tokenSecret, profile, done) => {
      process.env.TWI_ACCESS_TOKEN_KEY = token;
      process.env.TWI_ACCESS_TOKEN_SECRET = tokenSecret;
      const {
        id_str: user_twitter_id,
        id_str,
        screen_name,
        name,
        description,
        followers_count,
        friends_count,
        location,
        verified,
        statuses_count,
        ranking
      } = profile._json;

      const currentUser = await User.findOne({ user_twitter_id });
      // console.log({ currentUser });
      // create new user if the database doesn't have this user
      if (!currentUser) {
        const newUser = await new User({
          user_twitter_id,
          screen_name,
          name,
          description,
          followers_count,
          friends_count,
          location,
          verified,
          statuses_count,
          ranking
        }).save();
        if (newUser) {
          console.log('User has been created');
          done(null, newUser);
        }
      }
      done(null, currentUser);
    }
  )
);
