import Friend from '../models/friend';

export function insertManyFriends({ friends, options }) {
  return Friend.insertMany(friends, options);
}

export function findOneFriend(criteria) {
  return Friend.findOne(criteria);
}

export function createFriend(account) {
  return Friend.create(account);
}

export function findFriends({ condition, projection, options }) {
  return Friend.find(condition, projection, options);
}

export async function findFriendsPage({ condition, pageSize, pageNum }) {
  const skip = pageSize * pageNum - pageSize;
  const [aggregated] = await Friend.aggregate([
    {
      $facet: {
        data: [{ $match: {} }, { $skip: skip }, { $limit: pageSize }],
        totalCount: [{ $count: 'count' }]
      }
    }
  ]);
  const { data, totalCount } = aggregated;
  const total = totalCount[0].count;
  return { data, total };
  // const friends = await Friend.find(condition)
  //   .skip(pageSize * pageNum - pageSize)
  //   .limit(pageSize);

  // console.log({ id });
  // return friends;
}

// findFriendsPage({});

export function updateFriend({ criteria, value }) {
  return Friend.update(criteria, value);
}
