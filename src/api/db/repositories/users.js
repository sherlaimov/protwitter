import User from '../models/user';

export function findUsers({ condition, projection, options }) {
  return User.find(condition, projection, options);
}

export function findOneUser({ condition, projection, options }) {
  return User.findOne(condition, projection, options);
}
