import mongoose from '../mongo';

// TODO this has to be a complete user object
/*
{
  "screen_name": "sherlaimov",
  "id": "sherlaimov",
  "location": "Kharkov",
  "description": "Cosmopolitan. Aspiring Web Developer & Entrepreneur. Rollerskater. Atheist. Business Analyst @Truver_Inc. Originally from @Sloviansk (Tweeting in ENG & RU)",
  "followers_count": 650,
  "friends_count": 955,
  "verified": false,
  "statuses_count": 5441,
  "user_twitter_id": "263744389",
  "ranking": 1,
  "createdAt": "1533153670758",
  "updatedAt": "1533153670758"
}


    email: {
      type: String,
      required: true,
      trim: true,
      unique: true,
      match: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    },
    
*/
const UserSchema = new mongoose.Schema(
  {
    user_twitter_id: { type: String, required: true },
    screen_name: { type: String, required: true },
    name: String,
    description: String,
    followers_count: Number,
    friends_count: Number,
    location: String,
    verified: Boolean,
    statuses_count: Number,
    ranking: Number
  },
  { timestamps: true }
);

UserSchema.set('toJSON', { getters: true, virtuals: true });

UserSchema.statics.upsertTwitterUser = function(token, tokenSecret, profile, cb) {
  const that = this;
  return this.findOne(
    {
      'twitterProvider.id': profile.id
    },
    (err, user) => {
      // no user was found, lets create a new one
      if (!user) {
        const newUser = new that({
          email: profile.emails[0].value,
          screen_name: profile.username,
          twitterProvider: {
            id: profile.id,
            token,
            tokenSecret
          }
        });

        newUser.save((error, savedUser) => {
          if (error) {
            console.log(error);
          }
          return cb(error, savedUser);
        });
      } else {
        return cb(err, user);
      }
    }
  );
};

const userModel = mongoose.model('User', UserSchema);

export default userModel;
