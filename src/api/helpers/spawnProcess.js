import { spawn } from 'child_process';
import path from 'path';

const parserDir = path.resolve(__dirname, '../../friendshipParser');
const babelDir = path.resolve(__dirname, '../node_modules/.bin/babel-node');

const runningProcesses = new Set();

const spawnProcess = async ({ screenName, tokenKey, tokenSecret }) => {
  const options = {
    cwd: parserDir,
    env: { tokenKey, tokenSecret }
  };

  const child = spawn(babelDir, [`${parserDir}/cli.js`, '--screen_name', screenName], options);

  const { pid, spawnargs } = child;
  spawnargs.shift();
  const cliArg = spawnargs.shift();

  const processData = {
    pid,
    cli: cliArg,
    args: spawnargs.join(' ')
  };
  runningProcesses.add(processData);

  child.stdout.on('data', data => {
    console.log(`child stdout:\n${data}`);
  });

  // Writable stream in child process
  // child.stdin.on('data', data => {
  //   console.log('Standard in ===>', data);
  // });

  child.stderr.on('data', data => {
    console.error(`child stderr:\n${data}`);
  });
  child.on('exit', (code, signal) => {
    runningProcesses.delete(processData);
    console.log(`Running processes number -> ${runningProcesses.size}`);
    console.log(`Child process exited with code ${code} and signal ${signal}`);
  });
  child.on('close', (code, signal) => {
    runningProcesses.delete(processData);
    console.log(`Child process closed with code ${code} and signal ${signal}`);
  });
  child.on('error', e => {
    console.log('Child processed threw error');
    console.log(e);
  });
  console.log(`Running processes total => ${runningProcesses.size}`);
  await onExit(child);
};

function onExit(childProcess) {
  return new Promise((resolve, reject) => {
    childProcess.once('exit', (code, signal) => {
      if (code === 0) {
        resolve(undefined);
      } else {
        reject(new Error(`Exit with error code: ${code} and signal ${signal}`));
      }
    });
    childProcess.once('error', err => {
      reject(err);
    });
  });
}

export function getRunningProcesses() {
  return [...runningProcesses];
}

export default spawnProcess;
