const requestTokenUrl = 'http://localhost:3000/auth/twitter-signin';

const button = document.querySelector('#twitter-btn');

function getRequestToken() {
  const popup = openPopup();
  return window
    .fetch(requestTokenUrl, {
      method: 'GET',
      credentials: 'same-origin',
      headers: { 'Content-Type': 'application/json' }
    })
    .then(response => response.json())
    .then(data => {
      popup.location = `https://api.twitter.com/oauth/authenticate?oauth_token=${
        data.oauth_token
      }&force_login=${false}`;
      polling(popup);
    })
    .catch(error => {
      console.error(error);
    });
}

button.addEventListener('click', getRequestToken);

function openPopup() {
  const w = 600;
  const h = 400;
  const left = window.screen.width / 2 - w / 2;
  const top = window.screen.height / 2 - h / 2;
  const popup = window.open('', '', `width=${w}, height=${h}, top=${top}, left=${left}`);
  return popup;
}

function polling(popup) {
  const pollingHandle = setInterval(() => {
    const closeDialog = () => {
      clearInterval(pollingHandle);
      popup.close();
    };
    if (!popup || popup.closed || popup.closed === undefined) {
      clearInterval(pollingHandle);
      return;
    }
    if (
      popup.location.href.includes('oauth_token') &&
      popup.location.href.includes('oauth_verifier')
    ) {
      // popup.opener.location = 'http://localhost:3000/parsers.html';
      closeDialog();
    }
  }, 500);
}
