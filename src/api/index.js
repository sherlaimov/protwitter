import path from 'path';
import Koa from 'koa';
import Router from 'koa-router';
import bodyParser from 'koa-bodyparser';
import serve from 'koa-static';
import morgan from 'koa-morgan';
import passport from 'koa-passport';
import cors from '@koa/cors';
import session from 'koa-session';
import config from './config/config';
// import twiAuthRoute from './routes/twiAuth';
import parsersRoute from './routes/parsers';
import friendsRoute from './routes/friends';
import authRoutes from './routes/auth';
// import passportInit from './services/twiPassport';
import passportSetup from './config/passport-setup';
import ROUTES from './routes/routes';

// passportInit();
const app = new Koa();
const mainRouter = new Router();

// sessions
app.keys = ['super-secret-key'];
/*
      name: 'session',
      keys: [config.COOKIE_KEY],
      maxAge: 24 * 60 * 60 * 100
*/

// TODO Why exactly do we need session here?
app.use(session({ maxAge: 24 * 60 * 60 * 100 }, app));

// initalize passport
app.use(passport.initialize());
// deserialize cookie from the browser
app.use(passport.session());

app.use(cors());
app.use(bodyParser());
app.use(morgan('combined'));

app.use(serve(path.resolve(__dirname, '../', 'client')));
// app.use(serve(`${__dirname}/public`));

app.use(async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    ctx.body = { message: err.message };
    ctx.status = err.status || 500;
  }
});

const redirectToIndex = async (ctx, next) => {
  if (ROUTES.includes(ctx.request.path)) {
    console.log('redirectToIndex');
    ctx.request.path = '/';
    console.log(ctx.request.path);
    // ctx.redirect('/');
  }
  await next();
};

const authCheck = async (ctx, next) => {
  console.log('authCheck');
  const { user } = ctx.state;
  console.log(ctx.session);
  // console.log(ctx.state);
  if (!user) {
    ctx.set(401);
    ctx.body = JSON.stringify({
      authenticated: false,
      message: 'user has not been authenticated'
    });
  } else {
    await next();
  }
};

const isAuthenticated = async (ctx, next) => {
  console.log('***** AUTHENICATED *****');
  console.log(ctx.isAuthenticated());
  if (ctx.isAuthenticated()) {
    console.log('***** AUTHENICATED *****');
    await next();
  }
  ctx.redirect('/');
};

// AUTH ROUTES
// mainRouter.use('/auth', authRoutes.routes(), authRoutes.allowedMethods());
mainRouter.use('/auth', authRoutes.routes(), authRoutes.allowedMethods());

// app.use(isAuthenticated);
app.use(redirectToIndex);

// mainRouter.use('/auth', twiAuthRoute.routes(), twiAuthRoute.allowedMethods());
mainRouter.use('/friends', friendsRoute.routes(), friendsRoute.allowedMethods());
mainRouter.use('/parsers', parsersRoute.routes(), parsersRoute.allowedMethods());

// authCheck
mainRouter.use('/', async ctx => {
  console.log('/ THIS NEVER RUNS');
  ctx.set(200);
  ctx.body = { m: true };
  // ctx.body = {
  //   authenticated: true,
  //   message: 'user successfully authenticated',
  //   user: ctx.user,
  //   cookies: ctx.cookies
  // };
});

app.use(mainRouter.allowedMethods());
app.use(mainRouter.routes());

app.on('error', err => {
  console.log('server error', err);
});

app.listen(config.port, () => {
  console.log(`Listening on port ${config.port}`);
});

export default app;
