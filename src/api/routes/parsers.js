import Router from 'koa-router';
import { getRunningProcesses } from '../helpers/spawnProcess';

const router = new Router();

router.get('/', async ctx => {
  const processes = getRunningProcesses();
  ctx.set(200);
  ctx.body = processes;
});

export default router;
