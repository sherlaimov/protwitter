import passport from 'koa-passport';
import Router from 'koa-router';

const router = new Router();
const CLIENT_HOME_PAGE_URL = 'http://localhost:8080';

// when login is successful, retrieve user info
router.get('/login/success', async ctx => {
  console.log('********** LOGIN SUCCESS *************');
  const { user } = ctx.state;
  if (user) {
    ctx.set(200);
    // ctx.redirect(`http://localhost:8080/graph/${user.user_twitter_id}`);
    ctx.redirect('http://localhost:8080/graph');
    ctx.body = {
      success: true,
      message: 'user has successfully authenticated',
      user
      // user: ctx.user,
      // cookies: ctx.cookies
    };
  }
});

// when login failed, send failed msg
router.get('/login/failed', async ctx => {
  console.log('FAILED');
  ctx.set(401);
  ctx.body = {
    success: false,
    message: 'user failed to authenticate.'
  };
});

// When logout, redirect to client
router.get('/logout', async ctx => {
  // req.logout();
  ctx.logout();
  ctx.redirect(CLIENT_HOME_PAGE_URL);
});

// auth with twitter
router.get('/twitter', passport.authenticate('twitter'));

// redirect to home page after successfully login via twitter
router.get(
  '/twitter/redirect',
  passport.authenticate('twitter', {
    // successRedirect: CLIENT_HOME_PAGE_URL,
    successRedirect: '/auth/login/success',
    // successRedirect: 'http://localhost:8080',
    failureRedirect: '/auth/login/failed'
  })
);

module.exports = router;
