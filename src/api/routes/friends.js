import Router from 'koa-router';
import { findFriends, findFriendsPage } from '../db/repositories/friends';

const router = new Router();

// TODO we need to get user's process.env.twi_id
router.get('/', async ctx => {
  const pageNum = parseInt(ctx.query.pageNum, 10);
  const pageSize = parseInt(ctx.query.pageSize, 10);
  // console.log(page);
  // :userId
  // const processes = getRunningProcesses();
  // const condition = { from_friends_list_id: process.env.twi_id };
  // const pageSize = 10;
  const condition = { from_friends_list_id: '263744389' };
  try {
    // const friends = await findFriends({ condition });
    const { data, total } = await findFriendsPage({ condition, pageSize, pageNum });
    ctx.set(200);
    // pageSize
    ctx.body = { data, pageNum, total };
  } catch (e) {
    console.log(e);
  }
});

export default router;
