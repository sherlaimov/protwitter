export function listenTo({ event, selector, handler }) {
  document.addEventListener(event, e => {
    if (e.target.matches(selector)) {
      handler(e);
      renderGame(appState);
    }
  });
}
// (type: string, attr?: object, textNode?: string) :=> HTMLElement
export const createEl = (type, attr, textNode) => {
  const el = document.createElement(type);
  if (attr && Object.entries(attr).length !== 0) {
    Object.entries(attr).forEach(keyVal => {
      const [key, value] = keyVal;
      el.setAttribute(key, value);
    });
  }
  if (textNode) {
    el.textContent = textNode;
  }
  return el;
};
