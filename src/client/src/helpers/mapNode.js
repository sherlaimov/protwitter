const mapUser = ({
  description,
  followers_count,
  followsBack,
  friends_count,
  location,
  ranking,
  screen_name,
  statuses_count,
  name,
  verified,
  avatarUrl
}) => ({
  description,
  followers_count,
  followsBack,
  friends_count,
  location,
  ranking,
  screen_name,
  statuses_count,
  name,
  verified,
  avatarUrl
});

export default mapUser;
