import Mediator from 'mediator-js';

const MediatorSingleton = (function Singleton() {
  let instance;

  function createInstance() {
    return new Mediator();
  }

  return {
    getInstance() {
      if (!instance) {
        instance = createInstance();
      }
      return instance;
    }
  };
})();

const mediatorInstance = MediatorSingleton.getInstance();
export default mediatorInstance;
