import * as d3 from 'd3';
import tooltipTemplate from './tooltip.hbs';
import mapNode from '../../helpers/mapNode';
import mediator from '../../helpers/mediator-init';
import './tooltip.scss';

export default function tooltip(data, chart) {
  return selection => {
    function mouseover(d) {
      const tooltipHtml = tooltipTemplate(mapNode(d));
      
      const tooltip$ = d3
      .select('body')
      .append('div')
      .classed('tooltip', true)
      .attr('id', 'tooltip')
      .style('opacity', 0); // start invisible
      
      const positionLeft = () => {
        // left offsite
        if (d3.event.pageX < d3.select('#tooltip').node().offsetWidth) {
          return `${d3.event.pageX}px`;
        }
        if (d3.event.pageX > window.innerWidth - d3.select('#tooltip').node().offsetWidth) {
          return `${d3.event.pageX - d3.select('#tooltip').node().offsetWidth}px`;
        }
        // center
        return `${d3.event.pageX - d3.select('#tooltip').node().offsetWidth / 2}px`;
      };
      
      const positionTop = () => {
        if (d3.event.pageY < 500) {
          return `${d3.event.pageY + 40}px`;
        }
        return `${d3.event.pageY - d3.select('#tooltip').node().offsetHeight - 70}px`;
      };
      
      tooltip$
      .html(tooltipHtml)
      .style('left', positionLeft)
      .style('top', positionTop);
      
      tooltip$
      .transition()
      .duration(300)
      .style('opacity', 1); // show the tooltip
    }
    
    function mousemove() {
      const mouse = d3.mouse(chart.node());
      d3.select('#tooltip').attr('transform', `translate(${mouse[0] + 15},${mouse[1] + 20})`);
    }
    
    function mouseout() {
      d3.select('#tooltip').remove();
    }
    function onNodeRemove() {
      d3.select('#tooltip').remove();
    }
    mediator.subscribe('remove-node', onNodeRemove);
    
    selection
      .on('mouseover.tooltip', mouseover)
      .on('mousemove.tooltip', mousemove)
      .on('mouseout.tooltip', mouseout);
  };
}
