import * as d3 from 'd3';
// import * as legend from 'd3-svg-legend';
import chartFactory from '../helpers/d3';
import tooltip from './tooltip/tooltip';

import mediator from '../helpers/mediator-init';

const colors = d3.scaleOrdinal(d3.schemeDark2);

const protoChart = {
  width: window.innerWidth,
  height: window.innerHeight,
  margin: { left: 50, right: 50, top: 50, bottom: 50 },
  padding: { left: 10, right: 10, top: 10, bottom: 10 },
  id: 'graph'
};
const graphChart = {};

graphChart.init = function initChart(container) {
  Object.entries(protoChart).forEach(entry => {
    const [prop, val] = entry;
    this[`${prop}`] = val;
  });

  mediator.subscribe('remove-node', this.removeNode.bind(this));

  this.svg = d3
    // .select('body')
    .select(container)
    .append('svg')
    .attr('id', this.id || 'chart')
    .attr('width', this.width - this.margin.right)
    .attr('height', this.height - this.margin.bottom);

  this.container = this.svg
    .append('g')
    .attr('id', 'container')
    .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);

  this.zoom = d3.zoom().on('zoom', () => {
    this.container.attr('transform', d3.event.transform);
  });

  this.zoom(this.svg);
  this.svg.on('dblclick.zoom', null);

  this.link = this.container.append('g').selectAll('.link');
  this.node = this.container.selectAll('.node');

  // TODO
  // https://github.com/ericsoco/d3-force-attract
  this.simulation = d3
    .forceSimulation()
    .force(
      'link',
      d3
        .forceLink()
        .id(d => d.user_twitter_id)
        .distance(d => this.rScale(d.target.followers_count) * 2)
        .strength(0.2)
    )
    .force('charge', d3.forceManyBody().strength(-500))
    .force('center', d3.forceCenter(this.width / 2, this.height / 2));

  this.simulation.on('end', () => {
    console.log('end event');
  });
  // this.force(data);
};

graphChart.force = function Force(_data) {
  this.data = _data;
  const { nodes, links } = _data;
  console.log({ nodes });
  const t = d3.transition().duration(1000);
  const that = this;

  this.rScale = d3
    .scaleSqrt()
    .domain([0, d3.max(nodes, d => d.friends_count)])
    .range([1, 50]);
  window.rScale = this.rScale;

  this.link = this.link.data(links);
  this.link = this.link
    .enter()
    .append('line')
    .call(link => {
      link.transition().attr('stroke-opacity', 1);
    })
    .attr('class', 'link')
    .merge(this.link);

  this.link
    .exit()
    .transition()
    .attr('stroke-opacity', 0)
    .attrTween('x1', d => () => d.source.x)
    .attrTween('x2', d => () => d.target.x)
    .attrTween('y1', d => () => d.source.y)
    .attrTween('y2', d => () => d.target.y)
    .remove();

  this.node = this.node.data(nodes, d => d.user_twitter_id);
  // this.node = this.node.data(nodes, d => d.id);

  this.node
    .exit()
    .transition()
    .duration(500)
    .style('opacity', 0)
    .remove();

  const nodeEnter = this.node
    .enter()
    .append('g')
    .attr('class', d => `node ${d.screen_name}`)
    .each(this.appendNodes);

  this.node = nodeEnter.merge(this.node);

  this.node.call(
    d3
      .drag()
      .on('start', d => {
        this.dragStarted(d, this.simulation, this);
      })
      .on('drag', this.dragged.bind(this))
      .on('end', this.dragEnded.bind(this))
  );

  // Tooltip
  this.node.call(tooltip(d => d, this.container));

  // Node events
  this.node.on('click', this.getNodeData);
  this.node.on('dblclick', this.releaseNode);
  // this.node.on('dblclick.zoom', this.center.bind(this));
  this.simulation.on('tick', this.ticked.bind(this));
  this.simulation.nodes(nodes);
  this.simulation.force('link').links(links);
  // this.zoom.translateTo(this.svg, this.width / 2, this.height / 2);
  this.simulation.alphaTarget(0.3).restart();
};

graphChart.removeNode = function removeNode(d) {
  console.log('removeNode');
  console.log(d);
  const { user_twitter_id: nodeId } = d;
  const { nodes: oldNodes, links: oldLinks } = this.data;

  // mediator.publish('node-data', d);
  const newNodes = oldNodes.filter(node => node.user_twitter_id !== nodeId);
  const newLinks = oldLinks.filter(link => link.target.user_twitter_id !== nodeId);
  console.log(newLinks);
  // mediator.emit('remove-node');
  this.force({ nodes: newNodes, links: newLinks });
};
graphChart.getNodeData = function getNodeData(d) {
  mediator.publish('node-data', d);
};

graphChart.appendNodes = function appendNodes(d, i) {
  const currNode = d3.select(this);
  currNode
    .append('circle')
    // .transition(t)
    .attr('r', () => rScale(d.followers_count))
    // .attr('r', () => d.followers_count / 100)
    .style('fill', () => colors(i));

  currNode
    .append('text')
    .text(() => d.name || d.screen_name)
    // Automatic Text Sizing
    .style('font-size', function size() {
      const textWidth = this.getComputedTextLength();
      const parentWidth = this.parentNode.getBBox().width;
      let scale = 16;
      scale = ((parentWidth - 30) / textWidth) * 16;
      if (scale < 16) scale = 16;
      return `${scale}px`;
    })
    .attr('text-anchor', 'middle')
    .attr('y', 5);
};

graphChart.ticked = function ticked() {
  this.link
    .attr('x1', d => d.source.x)
    .attr('y1', d => d.source.y)
    .attr('x2', d => d.target.x)
    .attr('y2', d => d.target.y);

  // node.attr('cx', d => d.x).attr('cy', d => d.y);
  // since now we are dealing with the g SVG element
  this.node.attr('transform', d => `translate(${d.x}, ${d.y})`);
};

graphChart.center = function center(d) {
  d3.event.stopPropagation();
  console.log(d);
  console.log(this.zoom.translateTo(this.svg, d.x, d.y));
};
graphChart.dragStarted = (d, simulation, node) => {
  d3.select(node).raise();
  if (!d3.event.active) simulation.alphaTarget(0.3).restart();
  // d.fixed = true;
  d.fx = d.x;
  d.fy = d.y;
};
graphChart.dragged = d => {
  d.fx = d3.event.x;
  d.fy = d3.event.y;
};

graphChart.dragEnded = function dragEnded(d) {
  if (!d3.event.active) this.simulation.alphaTarget(0);
  // not setting these values to null keeps node where you left it
  // d.fx = null;
  // d.fy = null;
};

graphChart.releaseNode = d => {
  d.fx = null;
  d.fy = null;
};

export default graphChart;
