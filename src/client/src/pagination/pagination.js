import mediator from '../helpers/mediator-init';
import paginationTemplate from './pagination.hbs';

const pagination = ({ total, pageNum, pageSize, container }) => {
  const pageCnt = Math.ceil(total / pageSize);

  // const container = document.querySelector('#pagination');
  const pageHtml = paginationTemplate({
    pagination: {
      page: pageNum, // The current page the user is on
      pageCount: pageCnt // The total number of available pages
    }
  });
  container.innerHTML = pageHtml;

  container.querySelectorAll('li').forEach(li => {
    li.addEventListener('click', e => {
      const page = parseInt(li.dataset.page, 10);
      mediator.publish('page-data', page);
    });
  });
};

export default pagination;
