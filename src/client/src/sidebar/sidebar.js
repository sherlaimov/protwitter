import mediator from '../helpers/mediator-init';
import sidebarTemplate from './sidebar.hbs';
import mapNode from '../helpers/mapNode';
import './sidebar.scss';

const sidebarState = {
  open: true,
  nodeData: null
};
export default function sidebar(state = sidebarState) {
  const { open } = state;
  console.log(this);

  const registerEvents = sidebarNode => {
    const button = sidebarNode.querySelector('#follow-unfollow');
    button.addEventListener('click', d => {
      mediator.emit('remove-node', sidebarState.nodeData);
    });
    button.addEventListener('mouseover', d => {
      button.textContent = 'Unfollow';
    });

    button.addEventListener('mouseout', d => {
      button.textContent = 'Following';
    });
  };

  mediator.subscribe('node-data', d => {
    sidebarState.nodeData = d;
    const section = document.querySelector('#sidebar');
    const newSection = document.createElement('section');
    newSection.id = 'sidebar';
    if (open) {
      section.style.display = 'block';
    }
    const sidebarHtml = sidebarTemplate(mapNode(d));
    newSection.innerHTML = sidebarHtml;
    registerEvents(newSection);
    section.replaceWith(newSection);
  });
}
