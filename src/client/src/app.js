import nanorouter from 'nanorouter';
import graphPage from './pages/graph/index';
import homePage from './pages/home/home';

import './main.scss';

const router = nanorouter({ default: '/404' });

window.router = router;

router.on('/graph', params => {
  graphPage.init();
});

router.on('/', params => {
  // graphPage.init();
  homePage.init();
});

const pathName = '/';

const poppin = () => {
  const { pathname } = window.location;
  router.emit(pathname);
};

// window.history.replaceState({}, pathName, pathName);
// window.history.pushState({}, pathName, window.location.origin + pathName);
router.emit(window.location.pathname);
window.addEventListener('popstate', poppin);
