import graphChart from '../../graph/force-fn2';
import { getGraphData } from '../../controllers/graphController';
import navTemplate from './nav.hbs';
import { createEl } from '../../helpers/basic';
import '../../pagination/pagination.scss';

import sidebar from '../../sidebar/sidebar';
import pagination from '../../pagination/pagination';
import mediator from '../../helpers/mediator-init';

const graphPage = {
  rootDiv: document.querySelector('#root'),
  pageContainer: createEl('div', { id: 'graph-page' }),
  paginationContainer: createEl('div', { id: 'pagination' }),
  sidebarContainer: createEl('section', { id: 'sidebar' }),
  pathName: '/graph',
  pageSize: 200,
  init() {
    this.rootDiv.innerHTML = '';
    this.pageContainer.innerHTML = navTemplate();
    sidebar();
    graphChart.init(this.pageContainer);
    mediator.subscribe('page-data', this.renderGraph.bind(this));
    this.renderPage().then(() => console.log('page rendered'));
  },
  async renderPage() {
    await this.renderGraph();
    this.pageContainer.appendChild(this.paginationContainer);
    this.pageContainer.appendChild(this.sidebarContainer);
    this.rootDiv.appendChild(this.pageContainer);
  },
  async renderGraph(pageNum = 1) {
    const { userSet, pageSize, paginationContainer } = this;
    const { data, pageNum: newPageNum, total } = await getGraphData({
      userSet: 'all_friends',
      pageSize,
      pageNum
    });

    // pagination({ total, pageNum: newPageNum, pageSize });
    pagination({ total, pageNum: newPageNum, pageSize, container: paginationContainer });
    graphChart.force(data);
  }
};

export default graphPage;
