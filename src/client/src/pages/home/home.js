import homeTemplate from './home.hbs';

const indexPage = {
  rootDiv: document.querySelector('#root'),
  init() {
    this.rootDiv.innerHTML = '';
    this.rootDiv.innerHTML = homeTemplate();
    this.registerEvents(this.rootDiv);
  },
  registerEvents(rootNode) {
    const button = rootNode.querySelector('#twi-auth');
    button.addEventListener('click', d => {
      window.open('http://localhost:3000/auth/twitter', '_self');
      // window.router.emit('/graph');
      // window.history.pushState({}, '/graph', `${window.location.origin  }/graph`);
    });
  }
};

export default indexPage;
