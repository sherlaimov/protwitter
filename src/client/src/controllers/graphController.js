import sherlaimovData from '../../data/rootNode.json';
import { API_BASE_URL } from '../config';

const getRootNodeData = () => sherlaimovData;
const ROOT_NODE = getRootNodeData();
ROOT_NODE.user_twitter_id = ROOT_NODE.id;

export async function getFriends() {
  const getData = async () => {
    try {
      const friends = await fetch(`${API_BASE_URL}/friends`);
      // const data = await friends.json();
      const data = await friends.json();

      return data;
    } catch (e) {
      console.error(e);
      return undefined;
    }
  };
  const nodes = await getData();
  return nodes;
}

export async function gePaginatedFriends({ pageSize, pageNum }) {
  const getData = async () => {
    try {
      const friends = await fetch(
        `${API_BASE_URL}/friends/?pageNum=${pageNum}&pageSize=${pageSize}`
      );
      const data = await friends.json();

      return data;
    } catch (e) {
      console.error(e);
      return undefined;
    }
  };
  const result = await getData();
  return result;
}

export async function getGraphData({ userSet, pageSize, pageNum }) {
  const { data: nodes, pageNum: newPageNum, total } = await gePaginatedFriends({
    pageSize,
    pageNum
  });

  nodes.push(ROOT_NODE);

  const links = nodes.map(({ user_twitter_id }) => ({
    target: user_twitter_id,
    source: 'sherlaimov'
  }));

  switch (userSet) {
    case 'all_friends':
      console.log('all_friends');
      return { data: { nodes, links }, pageNum: newPageNum, total };
    case 'all_followers':
      console.log('all_followers');
      return followers;
    case 'mutual_following':
      console.log('mutual_following');
      return nodes.filter(friend =>
        followers.find(follower => friend.screen_name === follower.screen_name)
      );
    case 'exclusive_followers':
      return followers.filter(follower =>
        nodes.every(friend => friend.screen_name !== follower.screen_name)
      );
    case 'exclusive_friends':
      return nodes.filter(friend =>
        followers.every(follower => follower.screen_name !== friend.screen_name)
      );
    default:
      return nodes;
  }
}

export async function getFollowers() {
  const getData = async () => {
    try {
      const response = await fetch('../data/followers.json');
      const data = await response.json();
      const followers = data.map(follower => {
        const followerData = mapUserData(follower);
        return {
          ...followerData,
          id: follower.screen_name
        };
      });
      return followers;
    } catch (e) {
      console.log(e);
      return undefined;
    }
  };
  const nodes = await getData();
  return nodes;
}
