import rc from 'rc';

const APP_NAME = 'TWITTER_APP';

const mongoPort = process.env.MONGO_PORT || 27017;
const config = {
  port: process.env.PORT || 8080,
  mongo: {
    url: process.env.DATABASE_URL || `mongodb://localhost:${mongoPort}/twitter_app`
  }
};

export default rc(APP_NAME, config);
