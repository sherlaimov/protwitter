import minimist from 'minimist';
import friendshipParser from './index';

/* 
  USAGE:
   npm run parse:friends --  --screen_name <name>
*/

const args = minimist(process.argv.slice(2));

const { screen_name, id, key, secret } = args;
if (screen_name === undefined) {
  console.log('Screen name is required');
  console.log('Please, use \n npm run parse:friends --  --screen_name <name>');
  process.exit(0);
}
if (key !== undefined && secret !== undefined) {
  process.env.tokenKey = key;
  process.env.tokenSecret = secret;
  process.env.twi_id = id;
  process.env.screen_name = screen_name;
}

friendshipParser({ screen_name });
