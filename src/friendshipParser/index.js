import colors from 'colors';
// import config from './config';
import { getFriends, getFriendships } from './helpers/twitterGet';
// import accountRanker from './helpers/accountRanker';
import { insertManyFriends } from './db/repositories/friends';
import mapUsers from './helpers/mapUser';
// import getAndSaveImg from './helpers/imageHandler';
/* 
  USAGE:
   npm run parse:friends --  --screen_name <name>
*/

let parsedFriendsCounter = 0;

const friendshipParser = async _params => {
  const screenName = _params.screen_name;
  const params = {
    // max 100 because /friendships/lookup.json max is 100
    count: 100,
    skip_status: true,
    skip_user: true,
    include_user_entities: false,
    cursor: '-1',
    // cursor: '1643904392038668186',
    ..._params
  };

  const friendsListParams = {
    params,
    route: '/friends/list.json'
  };

  const { users, next_cursor_str: nextCursor } = await getFriends(friendsListParams);
  console.log({ nextCursor });

  console.log(`Users length -> ${users.length}`);
  if (users.length === 0) {
    console.log(`${screenName} has zero friends.`);
    process.exit(0);
    return;
  }
  // users.length === 0 ||
  if (nextCursor === '0') {
    console.log(`All friends'bios of ${params.screen_name} have been parsed`);
    console.log(`Friends parsed total => ${parsedFriendsCounter}`.green);
    process.exit(0);
    return;
  }
  parsedFriendsCounter += users.length;

  // Query Twitter API for connections
  const screenNamesString = users.map(friend => friend.screen_name).join(',');

  const friendshipsLookupParams = {
    params: { screen_name: screenNamesString },
    route: '/friendships/lookup.json'
  };

  const connectionsArr = await getFriendships(friendshipsLookupParams);
  console.log(`Are they equal? --> ${users.length === connectionsArr.length}`.rainbow);

  if (connectionsArr.length === 0) {
    console.log('Returned zero connections');
    return;
  }

  const mappedUsers = users.map(mapUsers).map(u => {
    const user = u;
    // following and followed_by
    const { connections } = connectionsArr.find(
      connection => user.user_twitter_id === connection.id_str
    );
    if (connections && connections.includes('followed_by')) {
      user.followsBack = true;
    } else {
      user.followsBack = false;
    }
    return user;
  });

  // const usersWithAvatarsPromise = await mappedUsers.map(getAndSaveImg);
  // const usersWithAvatars = await Promise.all(usersWithAvatarsPromise);
  // console.log(usersWithAvatars);
  try {
    // [options.ordered «Boolean» = true] if true, will fail fast on the first error encountere
    const inserted = await insertManyFriends(
      // { friends: mappedUsers },
      { friends: mappedUsers },
      { options: { ordered: true } }
    );
    console.log(`*** ==> Inserted ${inserted.length} user accounts <== ***`.bgBlue);
  } catch (e) {
    console.log('=> ERROR'.bgRed);
    console.log(e.message);
  }

  console.log(`Friends parsed total => ${parsedFriendsCounter}`);
  // if (parsedFriendsCounter >= 1) {
  //   process.exit(0);
  //   return;
  // }

  if (nextCursor !== 0) {
    setTimeout(friendshipParser, 500, { cursor: nextCursor, screen_name: screenName });
  }
};

export default friendshipParser;
