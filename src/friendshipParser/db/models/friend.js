import mongoose from '../mongo';

const { Schema } = mongoose;
mongoose.Promise = global.Promise;

const friendSchema = new Schema(
  {
    user_twitter_id: { type: String, required: true },
    screen_name: { type: String, required: true },
    name: String,
    description: String,
    followers_count: Number,
    friends_count: Number,
    location: String,
    verified: Boolean,
    statuses_count: Number,
    followsBack: Boolean,
    ranking: Number,
    twi_created_at: Date,
    from_friends_list_id: String,
    from_friends_list_name: String,
    // avatar: { data: Buffer, contentType: String }

    // avatar: Buffer
    avatarUrl: String
  },
  { timestamps: true }
);
friendSchema.index({ user_twitter_id: 1, from_friends_list: 1 }, { unique: true });
const friend = mongoose.model('friends', friendSchema);

export default friend;
