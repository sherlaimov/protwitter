import fetch from 'node-fetch';

export default async function getAndSaveImg(user) {
  const { avatarUrl, ...newUser } = user;
  const avatarUrlTarget = avatarUrl.replace('_normal', '');

  const response = await fetch(avatarUrlTarget);
  const imgBuf = await response.buffer();
  const b64 = imgBuf.toString('base64');
  const data = Buffer.from(b64, 'base64');
  // newUser.avatar = { data, contentType: 'image/png' };
  newUser.avatar = data;
  return newUser;
}
