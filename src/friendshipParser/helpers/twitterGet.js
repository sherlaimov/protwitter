import colors from 'colors';
import Twitter from 'twitter';
import config from '../config';

const hasProp = (o, prop) => Object.prototype.hasOwnProperty.call(o, prop);

let creds;
const initTwitter = (key, secret) => {
  const twitterKeys = {
    consumer_key: config.TWI_CONSUMER_KEY,
    consumer_secret: config.TWI_CONSUMER_SECRET,
    // access_token_key: process.env.tokenKey,
    access_token_key: key,
    // access_token_secret: process.env.tokenSecret
    access_token_secret: secret
  };
  creds = twitterKeys;
  return new Twitter(twitterKeys);
};

const sleep = timeUntilLimitResets => {
  console.log('The wait has started'.yellow);
  const milisecsToWait = (timeUntilLimitResets - Math.round(Date.now() / 1000)) * 1000 + 2000;
  console.log(`Parser will continue at ${new Date(timeUntilLimitResets * 1000)}`.yellow);
  return new Promise(ok => setTimeout(ok, milisecsToWait));
};

export const getFriends = async ({ route, params }) => {
  const twitter = initTwitter(process.env.tokenKey, process.env.tokenSecret);

  let timeOutInMins;

  let timeUntilLimitResets;
  let nextCursor;

  // _param is needed to pass a cursor when the query window opens
  const parseFriends = async _param => {
    console.log({ _param });
    const getDataParams = {
      ...params,
      ..._param
    };

    // console.log({ getDataParams });
    const friendsPromise = new Promise((resolve, reject) => {
      twitter.get(route, getDataParams, async (error, data, response) => {
        // ? hasProp(data, 'errors') || hasProp(data, 'error')
        if (error) {
          reject(error);
        }
        const { headers } = response;
        const { next_cursor_str } = data;
        nextCursor = next_cursor_str;
        const remainingReqNum = parseInt(headers['x-rate-limit-remaining'], 10);
        timeUntilLimitResets = parseInt(headers['x-rate-limit-reset'], 10);

        console.log(`Remaining requests number =>  ${remainingReqNum}`);
        if (remainingReqNum === 0) {
          await sleep(timeUntilLimitResets);
          resolve(parseFriends({ cursor: nextCursor }));
          return;
        }
        resolve(data);
      });
    });

    try {
      const result = await friendsPromise;
      return result;
    } catch (e) {
      console.log('ERROR OBJECT');
      console.log(e);
      const [error] = e;
      const { code, message } = error;
      if (code === 88) {
        if (timeOutInMins === undefined) {
          // 20 used to be a passed timeout value
          timeOutInMins = Math.round(Date.now() / 1000 / 60) + 30;
        }

        if (Math.round(Date.now() / 1000 / 60) >= timeOutInMins) {
          console.log('Giving up because of timeout'.red);
          return { data: [], status: 'error' };
        }
        await sleep(timeUntilLimitResets);
        return parseFriends({ cursor: nextCursor });
      }
      if (code === 89) {
        // Invalid or expired token.
        return { status: 'error', message };
      }
      console.log('----------> ERROR IN twitterGet');
      console.log(e);
      throw e;
    }
  };
  return parseFriends();
};

export const getFriendships = async ({ params, route }) => {
  const twitter = initTwitter(process.env.tokenKey, process.env.tokenSecret);
  let timeUntilLimitResets;

  return new Promise((resolve, reject) => {
    twitter.get(route, params, async (error, data, response) => {
      if (error || hasProp(data, 'errors') || hasProp(data, 'error')) {
        reject(error);
      }
      const { headers } = response;

      const remainingReqNum = parseInt(headers['x-rate-limit-remaining'], 10);
      timeUntilLimitResets = parseInt(headers['x-rate-limit-reset'], 10);

      console.log(`Remaining requests number =>  ${remainingReqNum}`);
      if (remainingReqNum === 0) {
        await sleep(timeUntilLimitResets);
        resolve(getFriendships({ params, route }));
        return;
      }
      resolve(data);
    });
  });
};

// DEPRECATED
const twitterGet = async requestParams => {
  const { route } = requestParams;

  if (route === '/friendships/lookup.json') {
    // dasds
  }

  if (route === '/friends/list.json') {
    const { users, next_cursor: nextCursor } = await getFriends(requestParams);

    if (users.length === 0) {
      console.log('Returned users array is empty');
      return { users, nextCursor };
    }

    return { users, nextCursor };
  }
  return { data: [], status: 'error', message: 'Return value has no match' };
};

export default twitterGet;
