import accountRanker from './accountRanker';

const mapUsers = ({
  name,
  screen_name,
  location,
  description,
  followers_count,
  friends_count,
  verified,
  statuses_count,
  created_at: twi_created_at,
  id_str: user_twitter_id,
  profile_image_url_https
}) => {
  const avatarUrl = profile_image_url_https.replace('_normal', '');
  return {
    name,
    avatarUrl,
    user_twitter_id,
    screen_name,
    location,
    description,
    followers_count,
    friends_count,
    verified,
    statuses_count,
    twi_created_at,
    ranking: accountRanker({ followers_count, friends_count, verified, statuses_count }),
    from_friends_list_name: process.env.screenName,
    from_friends_list_id: process.env.twi_id
  };
};
export default mapUsers;
